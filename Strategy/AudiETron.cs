﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    public class AudiETron : Machine
    {
        public AudiETron(IEngineSwitcher engineSwitcher, IAlarmSwircher alarmSwitcher)
        {
            base.engineSwitcher = engineSwitcher;
            base.alarmSwitcher = alarmSwitcher;
        }

        public override void About()
        {
            Console.WriteLine("To jest elektryczne Audi e-Tron");
        }
    }
}
