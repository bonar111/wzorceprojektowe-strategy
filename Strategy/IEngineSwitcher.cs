﻿using System;

namespace Strategy
{
    public interface IEngineSwitcher
    {
        void TurnOn();
    }

    public class PetrolEngineSwitcher : IEngineSwitcher
    {
        public void TurnOn()
        {
            Console.WriteLine("Uruchomiono silnik spalinowy");
        }
    }

    public class ElectricEngineSwitcher : IEngineSwitcher
    {
        public void TurnOn()
        {
            Console.WriteLine("Uruchomiono silnik elektryczny");
        }
    }
}
