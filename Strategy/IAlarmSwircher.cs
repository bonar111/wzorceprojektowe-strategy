﻿using System;

namespace Strategy
{
    public interface IAlarmSwircher
    {
        void TurnOn();
    }

    public class AlarmSwitcher : IAlarmSwircher
    {
        public void TurnOn()
        {
            Console.WriteLine("Uruchomiono alarm samochodowy");
        }
    }
}
