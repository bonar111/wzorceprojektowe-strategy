﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    public abstract class Machine
    {
        protected IEngineSwitcher engineSwitcher;
        protected IAlarmSwircher alarmSwitcher;

        public abstract void About();

        public void TurnOnEngine()
        {
            engineSwitcher.TurnOn();
        }

        public void TurnOnAlarm()
        {
            alarmSwitcher.TurnOn();
        }
    }
}
