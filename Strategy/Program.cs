﻿using System;

namespace Strategy
{
    public class Program
    {
        static void Main(string[] args)
        {
            Machine audiA3 = new AudiA3(new PetrolEngineSwitcher(), new AlarmSwitcher());

            audiA3.About();
            audiA3.TurnOnAlarm();
            audiA3.TurnOnEngine();

            Console.WriteLine();

            Machine audiETron = new AudiETron(new ElectricEngineSwitcher(), new AlarmSwitcher());

            audiETron.About();
            audiETron.TurnOnAlarm();
            audiETron.TurnOnEngine();
        }
    }
}
