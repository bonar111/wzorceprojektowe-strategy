﻿using System;

namespace Strategy
{
    public class AudiA3 : Machine
    {
        public AudiA3(IEngineSwitcher engineSwitcher, IAlarmSwircher alarmSwitcher)
        {
            base.engineSwitcher = engineSwitcher;
            base.alarmSwitcher = alarmSwitcher;
        }

        public override void About()
        {
            Console.WriteLine("To jest Audi A3");
        }
    }
}
